package ajalugu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Kirjuta {

	/*
	 * meetod, mis kirjutab programmist p2ritud informatsiooni, koos kuup2eva ja
	 * kellaajaga tekstifaili
	 */
	public static void faili(String tulemus) {

		try {

			String pealkiri = "| Kuupäev ja kellaaeg | Algus    | Lõpp     | km  |";
			File tekstiFail = new File("Otsingute ajalugu.txt");
			FileWriter loo = new FileWriter(tekstiFail.getAbsoluteFile(), true);
			BufferedReader loeRida = new BufferedReader(new FileReader(tekstiFail));
			BufferedWriter kriba = new BufferedWriter(loo);

			// kui faili pole, siis see luuakse
			if (!tekstiFail.exists()) {
				tekstiFail.createNewFile();
			} // if

			// kui pealkirja pole, siis see trykitakse ja eraldatakse joontega tekstist
			if (!pealkiri.equals(loeRida.readLine())) {
				kriba.write(pealkiri);
				kriba.newLine();
				for (int i = 0; i < pealkiri.length(); i++) {
					if (pealkiri.substring(i, i + 1).equals("|")) {
						kriba.write("|");
					} else {
						kriba.write("-");
					} // if
				} // for
				loeRida.close();
			} // if

			kriba.newLine();
			kriba.write(tulemus);
			kriba.close();

		} catch (IOException e) {
			System.out.println("(!) Ilmnes viga");
			e.printStackTrace();
		} // try

	} // faili meetod
} // Kirjutaja klass 