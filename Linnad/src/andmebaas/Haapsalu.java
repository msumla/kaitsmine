package andmebaas;

import java.util.HashMap;

public class Haapsalu {

	/*
	 * Erinevate linnade kaugused Haapsalust meetod v2ljastab soovitud teekonna
	 * kilomeetrite arvu
	 */
	@SuppressWarnings("rawtypes")
	public static Integer get(String algus, String l6pp) {
		HashMap<String, HashMap> haapsalust = new HashMap<String, HashMap>();
		HashMap<String, Integer> haapsalu = new HashMap<String, Integer>();
		haapsalust.put("haapsalu", haapsalu);
		haapsalu.put("tallinn", 108);
		haapsalu.put("tartu", 78);
		haapsalu.put("narva", 235);
		haapsalu.put("pärnu", 96);
		haapsalu.put("viljandi", 204);
		return haapsalu.get(l6pp);
	} // get meetod

} // Haapsalu klass