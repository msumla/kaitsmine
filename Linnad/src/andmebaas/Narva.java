package andmebaas;

import java.util.HashMap;

public class Narva {

	/*
	 * Erinevate linnade kaugused Narvast meetod v2ljastab soovitud teekonna
	 * kilomeetrite arvu
	 */	
	@SuppressWarnings("rawtypes")
	public static Integer get(String algus, String l6pp) {
		HashMap<String, HashMap> narvast = new HashMap<String, HashMap>();
		HashMap<String, Integer> narva = new HashMap<String, Integer>();
		narvast.put("narva", narva);
		narva.put("tallinn", 211);
		narva.put("tartu", 178);
		narva.put("pärnu", 291);
		narva.put("viljandi", 235);
		narva.put("haapsalu", 312);
		return narva.get(l6pp);
	} // get meetod
	
} // Narva klass