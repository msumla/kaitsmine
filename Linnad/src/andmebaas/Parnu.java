package andmebaas;

import java.util.HashMap;

public class Parnu {

	/*
	 * Erinevate linnade kaugused Pärnust meetod v2ljastab soovitud teekonna
	 * kilomeetrite arvu
	 */
	@SuppressWarnings("rawtypes")
	public static Integer get(String algus, String l6pp) {
		HashMap<String, HashMap> parnust = new HashMap<String, HashMap>();
		HashMap<String, Integer> parnu = new HashMap<String, Integer>();
		parnust.put("parnu", parnu);
		parnu.put("tallinn", 128);
		parnu.put("tartu", 174);
		parnu.put("narva", 291);
		parnu.put("viljandi", 96);
		parnu.put("haapsalu", 108);
		return parnu.get(l6pp);
	} // get meetod

} // Parnu klass