package andmebaas;

import java.util.HashMap;

public class Tallinn {

	/*
	 * Erinevate linnade kaugused Tallinnast meetod v2ljastab soovitud teekonna
	 * kilomeetrite arvu
	 */
	@SuppressWarnings("rawtypes")
	public static Integer get(String algus, String l6pp) {
		HashMap<String, HashMap> tallinnast = new HashMap<String, HashMap>();
		HashMap<String, Integer> tallinn = new HashMap<String, Integer>();
		tallinnast.put("tallinn", tallinn);
		tallinn.put("tartu", 186);
		tallinn.put("narva", 211);
		tallinn.put("pärnu", 128);
		tallinn.put("viljandi", 159);
		tallinn.put("haapsalu", 99);
		return tallinn.get(l6pp);
	} // get meetod

} // Tallinn klass