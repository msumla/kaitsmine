package andmebaas;

import java.util.HashMap;

public class Tartu {

	/*
	 * Erinevate linnade kaugused Tartust meetod v2ljastab soovitud teekonna
	 * kilomeetrite arvu
	 */
	@SuppressWarnings("rawtypes")
	public static Integer get(String algus, String l6pp) {
		HashMap<String, HashMap> tartust = new HashMap<String, HashMap>();
		HashMap<String, Integer> tartu = new HashMap<String, Integer>();
		tartust.put("tartu", tartu);
		tartu.put("tallinn", 186);
		tartu.put("narva", 178);
		tartu.put("pärnu", 174);
		tartu.put("viljandi", 78);
		tartu.put("haapsalu", 249);
		return tartu.get(l6pp);
	} // get meetod

} // Tartu klass
