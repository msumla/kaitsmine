package andmebaas;

import java.util.HashMap;

public class Viljandi {

	/*
	 * Erinevate linnade kaugused Viljandist meetod v2ljastab soovitud teekonna
	 * kilomeetrite arvu
	 */
	@SuppressWarnings("rawtypes")
	public static Integer get(String algus, String l6pp) {
		HashMap<String, HashMap> viljandist = new HashMap<String, HashMap>();
		HashMap<String, Integer> viljandi = new HashMap<String, Integer>();
		viljandist.put("viljandi", viljandi);
		viljandi.put("tallinn", 108);
		viljandi.put("tartu", 78);
		viljandi.put("narva", 235);
		viljandi.put("pärnu", 96);
		viljandi.put("haapsalu", 204);
		return viljandi.get(l6pp);
	} // get meetod

} // Viljandi klass