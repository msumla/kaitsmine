package systeem;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Meetod {

	private static String[] veeb;

	/* peameetod, mis k2ivitab sisendi- ja v2ljundimeetodi ning kuvab muud infot */
	public static void main(String[] args) {
		String[] s6it = new String[2];
		System.out.println("( i )  P r o g r a m m    k ä i v i t u s  !");
		System.out.println("--------------------------------------------");
		s6it = Sisend.tkValik(args);
		int kkJvE = Sisend.kkJahVoiEi(args);
		int kkLiik = Sisend.kkLiik(kkJvE, args);
		V2ljund.get(s6it[0], s6it[1], kkJvE, kkLiik);
		System.out.println("------------------------------------------");
		System.out.println("( i )  P r o g r a m m    l õ p e t a s  !");

	} // main

	/*
	 * meetod, mis kontrollib, kas sisestatud linna nimi on etteantud linnade
	 * massiivis olemas
	 */
	public static boolean nimeOtsing(String[] massiiv, String linnaNimi) {
		for (String s : massiiv) {
			if (s.equalsIgnoreCase(linnaNimi)) {
				return false;
			} // if
		} // for
		return true;
	} // nimeOtsing meetod

	/*
	 * meetod, mis prindib välja etteantud linnade valiku ja jätab välja juba
	 * sisestatud linna, sest seda ei saa uuesti valida
	 */
	public static void valikuteV2ljastus(String[] massiiv, String sone) {
		for (int i = 0; i < massiiv.length; i++) {
			if (!sone.equalsIgnoreCase(massiiv[i]) && i < massiiv.length - 1) {
				System.out.print(massiiv[i] + ", ");
			} else if (!sone.equalsIgnoreCase(massiiv[i])
					|| i == massiiv.length - 1) {
				System.out.print(massiiv[i]);
			} // if
		} // for

	} // valikuteV2ljastus meetod

	/* meetod, mis muudab kuvatava linna nime suure algustähega */
	public static String suurT2ht(String sona) {
		return Character.toUpperCase(sona.charAt(0)) + sona.substring(1);
	} // suurT2ht meetod

	/* meetod, mis muudab sisseütlevas käändes olevate sõnade lõppe */
	public static String t2heN2rija(String sona) {
		if (sona.equalsIgnoreCase("haapsalu")) {
			sona = sona.substring(0, sona.length() - 1) + "lu";
		} else if (sona.equalsIgnoreCase("tallinn")) {
			sona = sona + "asse";
		} else if (!sona.equalsIgnoreCase("haapsalu")) {
			sona = sona + "sse";
		} // if
		return sona;
	} // t2heN2rija meetod

	/*
	 * meetod, mis kysib kasutajalt, kas ta soovib lisaks arvutada kytusekulu
	 * ning tagastab selle reaalarvuna
	 */
	public static int kkValikud(String[] args) {

		String valik;
		int kkJvE = 0;
		do {
			valik = Sisend.sisse.next();
		} // do
		while ((valik.equalsIgnoreCase("j") || valik.equalsIgnoreCase("e")) == false);
		if (valik.equalsIgnoreCase("j")) {
			kkJvE = 1;
		} else if (valik.equalsIgnoreCase("e")) {
			kkJvE = 0;
		} // if

		return kkJvE;
	} // kkValikud meetod

	/*
	 * meetod, mis kysib kasutajalt, kas ta soovib kasutada bensiini v6i diislit
	 * ning tagastab selle reaalarvuna
	 */
	public static int kkLiikValikud(String[] args) {

		String valik;
		int kkLiik = 0;
		do {
			valik = Sisend.sisse.next();
		} // do
		while ((valik.equalsIgnoreCase("b") || valik.equalsIgnoreCase("d")) == false);
		if (valik.equalsIgnoreCase("b")) {
			kkLiik = 1;
		} else if (valik.equalsIgnoreCase("d")) {
			kkLiik = 0;
		} // if

		Sisend.sisse.close();

		return kkLiik;
	} // kkLiikValikud meetod

	/*
	 * meetod, mis prindib v2lja algus- ja l6pp-punkti nimed, erinevates
	 * seestytlevates k22netes
	 */
	public static void tkPrinter(String algus, String l6pp) {
		if (!algus.equalsIgnoreCase("tallinn")) {
			System.out.print("(@) " + suurT2ht(algus) + "st " + Meetod.suurT2ht(t2heN2rija(l6pp)) + " on ");
		} else if (algus.equalsIgnoreCase("tallinn")) {
			System.out.print("(@) " + suurT2ht(algus) + "ast " + Meetod.suurT2ht(t2heN2rija(l6pp)) + " on ");
		} // if
	} // printer meetod
	
	/*
	 * meetod, mis prindib teekonna pikkusele ja kytusehinnale vastavalt v2lja
	 * kytusekulu maksumuse
	 */
	public static void kkPrinter(int kkNum, int kkJvE, int kkLiik) {
		
		String[] hind = new String[2];
		hind = OtsiHinda.main(veeb);
		double bensuHind = Double.parseDouble(hind[0]);
		double dislaHind = Double.parseDouble(hind[1]);
		String kk = null;
		
		// kui kasutaja soovib kytusekulu leida, siis valitakse bensiini ja diisli vahel
		if (kkJvE == 1) {
			if (kkLiik == 1) {
				kk = "(@) Sõit maksab kõige rohkem " + Math.round((kkNum / 6) * bensuHind) + " Eurot, maksimaalne liitri hind on " + bensuHind + " Eurot.";
			} else if (kkLiik == 0) {
				kk = "(@) Sõit maksab kõige rohkem " + Math.round((kkNum / 6) * dislaHind) + " Eurot, maksimaalne liitri hind on " + dislaHind + " Eurot.";
			} // if
		} else if (kkJvE == 0) {
			kk = "";
		} // if
		System.out.println(kk);
	} // kkPrinter meetod

	/*
	 * meetod, mis tagastab antud hetke kuup2eva ja kellaaja ning algus- ja
	 * l6pp-punkti nimed, et neid logifaili trykkida
	 */
	public static String logija(String algus, String l6pp) {
		String paevJaKell;
		SimpleDateFormat kp = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
		paevJaKell = kp.format(Calendar.getInstance().getTime());
		return "| " + paevJaKell + " | " + String.format("%0$-8s", Meetod.suurT2ht(algus)) + " | " + String.format("%0$-8s", Meetod.suurT2ht(l6pp)) + " | ";
	} // logija meetod

} // Meetodid klass