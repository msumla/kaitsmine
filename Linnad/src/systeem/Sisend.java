package systeem;

import java.util.Scanner;

public class Sisend {

	// loon globaalse scanner tyypi sisendi objekti
	static Scanner sisse = new Scanner(System.in);
	
	/*
	 * meetod, mis kysib kasutajalt teekonna algus- ja l6pp-punkti ning tagastab
	 * need String tyypi massiivina
	 */
	public static String[] tkValik(String[] args) {

		// teekond objektid ja linnade valiku massiivi loomine
		String[] linnadeNimed = new String[] { "Tallinn", "Tartu", "Narva", "Pärnu", "Viljandi", "Haapsalu" };
		TeeTyyp teekond = new TeeTyyp(null, null);
		
		// alguspunkti sisestus, valikute kontrolliga
		System.out.print("(*) Missugusest linnast algab teie teekond? (Saadaval on ");
		Meetod.valikuteV2ljastus(linnadeNimed, "");
		System.out.println("): ");
		while (Meetod.nimeOtsing(linnadeNimed, teekond.algusPunkt = sisse.next().toLowerCase().trim())) {
			System.out.println("(!) Vale sisestus. Palun proovige uuesti:");
		} // while
		
		// l6pp-punkti sisestus, valikute- ja algpunktist erinevuse kontrolliga
		System.out.print("(*) Kuhu soovite soita? (Saadaval on ");
		Meetod.valikuteV2ljastus(linnadeNimed, teekond.algusPunkt);
		System.out.println("): ");
		while (Meetod.nimeOtsing(linnadeNimed, teekond.l6ppPunkt = sisse.next().toLowerCase().trim()) || teekond.algusPunkt.equalsIgnoreCase(teekond.l6ppPunkt) == true) {
			System.out.println("(!) Vale sisestus. Palun proovige uuesti:");
		} // while

		/* massiivi tagastus */
		String[] s6it = new String[2];
		s6it[0] = teekond.algusPunkt;
		s6it[1] = teekond.l6ppPunkt;
		return s6it;
	} // teekond meetod

	/*
	 * meetod, mis kysib kasutajalt, kas ta soovib leida kytusekulu hinda v6i
	 * l6petada programmi t88
	 */
	public static int kkJahVoiEi(String[] args) {
		System.out.println("(*) Kas soovite leida teekonna kütusekulu hinda? (J = Jah / E = Ei):");
		return Meetod.kkValikud(args);
	} // kkValik

	/*  */
	public static int kkLiik(int kkJvE, String[] args) {
		if (kkJvE == 1) {

			System.out.println("(*) Kas kasutate bensiini või diislit? (B = Bensiin / D = Diisel):");
			return Meetod.kkLiikValikud(args);
		}
		return 0;
	} // kkLiik meetod

} // Sisend klass