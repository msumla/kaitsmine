package systeem;

/* loon uue tyybi, mis sisaldab String tyypi s6nesid, 
 * mis v2ljendavad s6idu algus- ja l6pp-punkti 
 */

public class TeeTyyp {

	/* loon tyybile kaks String tyypi muutjat */
	String algusPunkt;
	String l6ppPunkt;

	/* loon s6nedele konstruktori */
	public TeeTyyp(String algusPunkt, String l6ppPunkt) {
		this.algusPunkt = algusPunkt;
		this.l6ppPunkt = l6ppPunkt;

	} // TeeTyyp konstuktor

	/* formaalne toString tagastusmeetod */
	@Override
	public String toString() {
		return algusPunkt + " ja " + l6ppPunkt;
	} // toString meetod

} // TeeTyyp klass
