package systeem;

import ajalugu.Kirjuta;
import andmebaas.*;

public class V2ljund {

	/*
	 * antud meetod p88rdub valitud linna hashmap-i poole ja v2ljastatab
	 * alguspunkti, l6pp-punkti ning nendevahelise teepikkuse nii konsooli, kui
	 * logifaili
	 */
	public static void get(String algus, String l6pp, int kkJvE, int kkLiik) {
		
		// p88rdutakse meetodi poole, mis prindib v2lja algus- ja l6pp-punkti nimed
		Meetod.tkPrinter(algus, l6pp);
		
		// vastavalt alguspinktile, valitakse, millise hashmap-i poole p88rduda
		switch (algus) {
		case "tallinn":
			System.out.println(Tallinn.get(algus, l6pp) + " km.");
			Meetod.kkPrinter(Tallinn.get(algus, l6pp), kkJvE, kkLiik);
			Kirjuta.faili(Meetod.logija(algus, l6pp) + String.format("%" + 3 + "s", Tallinn.get(algus, l6pp)) + " |");
			break;
		case "tartu":
			System.out.println(Tartu.get(algus, l6pp) + " km.");
			Meetod.kkPrinter(Tartu.get(algus, l6pp), kkJvE, kkLiik);
			Kirjuta.faili(Meetod.logija(algus, l6pp) + String.format("%" + 3 + "s", Tartu.get(algus, l6pp)) + " |");
			break;
		case "narva":
			System.out.println(Narva.get(algus, l6pp) + " km.");
			Meetod.kkPrinter(Narva.get(algus, l6pp), kkJvE, kkLiik);
			Kirjuta.faili(Meetod.logija(algus, l6pp) + String.format("%" + 3 + "s", Narva.get(algus, l6pp) + " |"));
			break;
		case "pärnu":
			System.out.println(Parnu.get(algus, l6pp) + " km.");
			Meetod.kkPrinter(Parnu.get(algus, l6pp), kkJvE, kkLiik);
			Kirjuta.faili(Meetod.logija(algus, l6pp) + String.format("%" + 3 + "s", Parnu.get(algus, l6pp)) + " |");
			break;
		case "viljandi":
			System.out.println(Viljandi.get(algus, l6pp) + " km.");
			Meetod.kkPrinter(Viljandi.get(algus, l6pp), kkJvE, kkLiik);
			Kirjuta.faili(Meetod.logija(algus, l6pp) + String.format("%" + 3 + "s", Viljandi.get(algus, l6pp)) + " |");
			break;
		case "haapsalu":
			System.out.println(Haapsalu.get(algus, l6pp) + " km.");
			Meetod.kkPrinter(Haapsalu.get(algus, l6pp), kkJvE, kkLiik);
			Kirjuta.faili(Meetod.logija(algus, l6pp) + String.format("%" + 3 + "s", Haapsalu.get(algus, l6pp)) + " |");
			break;
		} // switch

	} // get meetod
} // V2ljund klass